#jboss/wildfly
FROM kishore4122/img:v7
ADD samplejar/target/samplejar-1.0-SNAPSHOT.jar /opt/jboss/wildfly/standalone/deployment/
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0"]
